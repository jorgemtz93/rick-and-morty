import { BrowserRouter, Route, Routes } from "react-router-dom"
import Home from "./components/Home"
import Favorites from "./components/Favorites"
import Character from "./components/Character"
import Navbar from "./components/Navbar"
import { DataProvider } from "./contex/DataContext"

const App = () => {
  return (
    <DataProvider>
      <BrowserRouter>
        <main className="page">
          <Navbar />
          <div className="container">
            <h1 className="text-center">Rick and Morty API</h1>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/favoritos" element={<Favorites />} />
              <Route path="/character/:id" element={<Character />} />
            </Routes>
          </div>
        </main>
      </BrowserRouter>
    </DataProvider>
  )
}

export default App

import { useContext, useEffect } from "react"
import DataContext from "../contex/DataContext"
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom"
import useCharacterById from "../hooks/useCharacterById"
import Spinner from "./layout/Spinner"

const Character = () => {
  const { favorites, setFavorites } = useContext(DataContext)
  const params = useParams()
  const { isLoading, character } = useCharacterById(params.id)

  const addFavorites = () => {
    setFavorites([character, ...favorites])
  }

  return isLoading ? (
    <Spinner />
  ) : (
    <div className="container py-5">
      <div className="d-flex justify-content-between">
        <Link to="/" className="btn btn-primary w-auto">
          {"<"} Atras
        </Link>
        <button className="btn btn-secondary w-auto" onClick={addFavorites}>
          Agregar a favoritos
        </button>
      </div>
      <div className="text-center">
        <img src={character.image} alt="" className="w-30 character" />
        <div className="info">
          <h3>{character.name}</h3>
          <h6>
            <i
              className={`bi bi-circle-fill ${
                character.status === "Alive"
                  ? "text-success"
                  : character.status === "Dead"
                  ? "text-danger"
                  : "text-muted"
              }`}
            >
              {" "}
            </i>
            {character.status} - {character.species}
          </h6>
          <hr />
          <h5>Genero:</h5>
          <p>{character.gender}</p>
          <hr />
          <h5>Origen:</h5>
          <p>{character.origin.name}</p>
          <hr />
          <h5>Ultima ubicacion conocida:</h5>
          <p>{character.location.name}</p>
        </div>
      </div>
    </div>
  )
}

export default Character

import { Link } from "react-router-dom"

const DataItem = ({ item, addFavorites, favorite }) => {
  const handleFavorites = () => {
    addFavorites(item)
  }
  return (
    <div className="col-3 pb-5">
      <div className="card rounded">
        <Link
          className="text-decoration-none text-dark"
          to={`/character/${item.id}`}
        >
          <img src={item.image} alt={item.name} className="card-img-top" />
        </Link>
        <div className="card-body text-center">
          <h4 className="card-title mb-4">{item.name}</h4>
          <h6 className="card-subtitle">
            <i
              className={`bi bi-circle-fill ${
                item.status === "Alive"
                  ? "text-success"
                  : item.status === "Dead"
                  ? "text-danger"
                  : "text-muted"
              }`}
            >
              {" "}
            </i>
            {item.status} - {item.species}
          </h6>
          <button
            className={`btn btn-link ${
              favorite ? "text-warning" : "text-dark"
            }`}
            onClick={handleFavorites}
          >
            <i className="bi bi-star-fill"></i>
          </button>
        </div>
      </div>
    </div>
  )
}

export default DataItem

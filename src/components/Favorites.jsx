import { useContext, useEffect } from "react"
import DataContext from "../contex/DataContext"
import DataItem from "./DataItem"

const Favorites = () => {
  const { favorites } = useContext(DataContext)

  return favorites.length ? (
    <div>
      <div className="row">
        {favorites.map((item) => (
          <DataItem
            key={item.id}
            item={item}
            favorite={favorites.some((element) => element.id == item.id)}
          />
        ))}
      </div>
    </div>
  ) : (
    <div className="row justify-content-center mb-5 text-muted">
      <h3>No hay favoritos</h3>
    </div>
  )
}

export default Favorites

import { useContext } from "react"
import DataItem from "./DataItem"
import DataContext from "../contex/DataContext"
import ReactPaginate from "react-paginate"
import useCharacters from "../hooks/useCharacters"
import Spinner from "./layout/Spinner"
import { useEffect } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import queryString from "query-string"
import { useFormik } from "formik"

const Home = () => {
  const { isLoading, data, getCharacters } = useCharacters()

  const location = useLocation()
  const navigate = useNavigate()

  const { page = 0, name = "" } = queryString.parse(location.search)

  const { favorites, setFavorites } = useContext(DataContext)

  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: (values) => {
      formik.resetForm()
      if (values.search == "") {
        navigate("/?page=1&name=")
      } else {
        navigate(`/?page=1&name=${values.search}`)
      }
    },
  })

  const pageChange = (data) => {
    navigate(`/?page=${data.selected + 1}&name=${name}`)
  }

  const addFavorites = (card) => {
    const copy = structuredClone(card)
    setFavorites([copy, ...favorites])
  }

  useEffect(() => {
    if (!page) {
      navigate(`/?page=1&name=${name}`, { replace: true })
    } else {
      getCharacters(page, name)
    }
  }, [page, name])

  return isLoading ? (
    <Spinner />
  ) : (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <div className="row mb-5">
          <div className="col-10">
            <input
              id="search"
              name="search"
              type="text"
              className="form-control"
              placeholder="Busca a un personaje"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
          </div>
          <div className="col-2">
            <button
              className="btn btn-primary btn-sm h-100 w-100"
              type="submit"
            >
              Buscar
            </button>
          </div>
        </div>
      </form>
      <div className="row">
        {data.results.map((item) => (
          <DataItem
            key={item.id}
            item={item}
            addFavorites={addFavorites}
            favorite={favorites.some((element) => element.id == item.id)}
          />
        ))}
      </div>

      <div className="row justify-content-center mb-5">
        <ReactPaginate
          className="pagination justify-content-center my-4 gap-4"
          pageCount={data.info?.pages}
          forcePage={page === 1 ? 0 : page - 1}
          previousLabel="&laquo;"
          nextLabel="&raquo;"
          activeClassName="active"
          onPageChange={pageChange}
        />
      </div>
    </div>
  )
}

export default Home

import { Link } from "react-router-dom"

const Navbar = () => {
  return (
    <div className="navbar-dark bg-dark subnavbar">
      <div className="container">
        <div className="d-flex justify-content-end">
          <Link
            to="/"
            className="btn text-white btn-link text-decoration-none w-auto"
          >
            Inicio
          </Link>
          <Link
            to="/favoritos"
            className="btn text-white btn-link text-decoration-none w-auto"
            id="favorites"
          >
            <i className="bi bi-star-fill"></i> Favoritos
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Navbar

import spinner from "../assets/spinner.gif"

const Spinner = () => {
  return (
    <div className="container">
      <div className="d-flex justify-content-end p-5">
        <img
          src={spinner}
          alt="Loading..."
          width={180}
          className="text-center mx-auto"
        />
      </div>
    </div>
  )
}

export default Spinner

import { createContext, useState, useEffect } from "react"

const DataContext = createContext()

export const DataProvider = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState([])
  const [character, setCharacter] = useState([])
  const [pageNumber, setPageNumber] = useState(1)
  const [favorites, setFavorites] = useState([])

  useEffect(() => {
    fetchData(pageNumber)
  }, [pageNumber])

  const fetchData = async () => {
    const response = await fetch(
      `https://rickandmortyapi.com/api/character/?page=${pageNumber}`
    )
    const data = await response.json()

    setData(data)
    setIsLoading(false)
  }

  const getCharacter = async (id) => {
    const response = await fetch(
      "https://rickandmortyapi.com/api/character/" + id
    )
    const character = await response.json()

    setCharacter(character)
    setIsLoading(false)
  }

  return (
    <DataContext.Provider
      value={{
        data,
        isLoading,
        character,
        pageNumber,
        favorites,
        getCharacter,
        fetchData,
        setPageNumber,
        setFavorites,
      }}
    >
      {children}
    </DataContext.Provider>
  )
}

export default DataContext

import { useEffect, useState } from "react"

const useCharacterById = (id) => {
  const [isLoading, setIsLoading] = useState(true)
  const [character, setCharacter] = useState(null)

  const getCharacterById = async (id) => {
    setIsLoading(true)
    const response = await fetch(
      "https://rickandmortyapi.com/api/character/" + id
    )
    const character = await response.json()

    setCharacter(character)
    setIsLoading(false)
  }

  useEffect(() => {
    getCharacterById(id)
  }, [id])

  return {
    isLoading,
    character,
  }
}

export default useCharacterById

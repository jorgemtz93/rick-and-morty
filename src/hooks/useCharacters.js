import { useEffect, useState } from "react"

const useCharacters = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState(null)

  const getCharacters = async (pageNumber, name) => {
    setIsLoading(true)
    const response = await fetch(
      `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${name}`
    )
    const data = await response.json()

    setData(data)
    setIsLoading(false)
  }

  return {
    isLoading,
    data,
    setData,
    getCharacters,
  }
}

export default useCharacters
